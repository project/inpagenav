<?php

namespace Drupal\inpagenav\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure inpage settings for this site.
 */
class InpageSettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'inpage.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'inpage_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $form['parent_wrappper'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add Class name for the parent container wrapper div in which all H tags are present.'),
      '#default_value' => $config->get('parent_wrappper')
    
    ];
    $form['tag_wrapper'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Add name of the wrapper classes for the h tags which should not be included in the page navigation.'),
      '#default_value' => $config->get('tag_wrapper')
    
    ]; 
    
    $form['card_tag_wrapper'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Add name of the wrapper classes for the h tags which are inside a card component structure and which should not be included in the page navigation.'),
      '#default_value' => $config->get('card_tag_wrapper')
    
    ]; 
 
    $form['tags_to_include'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Add type of h tags to include like h1, h2 or h3 seperated by a comma'),
      '#default_value' => $config->get('tags_to_include')
    
    ]; 

    return parent::buildForm($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
   
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('parent_wrappper', $form_state->getValue('parent_wrappper'))
      ->set('tag_wrapper', $form_state->getValue('tag_wrapper'))
      ->set('tags_to_include', $form_state->getValue('tags_to_include'))
      ->set('card_tag_wrapper', $form_state->getValue('card_tag_wrapper'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}