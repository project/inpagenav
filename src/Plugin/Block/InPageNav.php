<?php

namespace Drupal\inpagenav\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block with a inpage navigation.
 *
 * @Block(
 *   id = "inpage",
 *   admin_label = @Translation("Inpage Navigation Block"),
 * )
 */

class InPageNav extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $configsettings = \Drupal::config('inpage.settings');
    $parent_wrappper = ($configsettings->get('parent_wrappper')) ? $configsettings->get('parent_wrappper') : '';
    $tag_wrapper = ($configsettings->get('tag_wrapper')) ? $configsettings->get('tag_wrapper') : '';
    $tags = ($configsettings->get('tags_to_include')) ? $configsettings->get('tags_to_include') : '';
    $card_wrapper = ($configsettings->get('card_tag_wrapper')) ? $configsettings->get('card_tag_wrapper') : '';
    return [
        '#theme' => 'inpagenav',
        '#attached' => array(
            'library' => array('inpagenav/inpagenav'),
            'drupalSettings' => ['inpage_parent_wrappper' =>  $parent_wrappper, 'tag_wrapper' => $tag_wrapper, 'tag' => $tags, 'card_tag_wrapper' => $card_wrapper]
            ),
      ];
  }

}